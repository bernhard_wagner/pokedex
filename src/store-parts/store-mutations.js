import mutationTypes from '../const/mutationTypes'

export default {
  [mutationTypes.SET_POKELIST]: (state, list) => {
    state.pokemonList = list;
  },
  [mutationTypes.SET_CURRENT_POKEMON]: (state, pokemon) => {
    state.currentPokemon = pokemon;
  },
  [mutationTypes.SET_CURRENT_POKEMON_LOADED]: (state, isLoaded) => {
    state.currentPokemonLoaded = isLoaded;
  }
}
