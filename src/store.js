import Vue from 'vue'
import Vuex from 'vuex'
import StoreGetters from './store-parts/store-getters'
import StoreActions from './store-parts/store-actions'
import StoreMutations from './store-parts/store-mutations'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    pokemonList: [/* */],
    currentPokemon: {/*  */},
    currentPokemonLoaded: false
  },
  getters: StoreGetters,
  actions: StoreActions,
  mutations: StoreMutations,

})
