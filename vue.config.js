module.exports = {
  lintOnSave: false,

  //here you can config your plugins (and webpack)

  chainWebpack: (config) => { //hooks in module rules and change settings
    config.module.rules.delete('svg'); //deletes svg rule...not needed anymore
    config.module.rule('images')
      .test(/\.(svg|png|jpe?g|gif|webp)(\?.*)?$/); //override test to add the svg so svg will be treated like other images
  },

  pwa: {
    themeColor: '#BC3333',
    name: 'Pokemon Pokedex',
    workboxOptions: {
      cacheId: 'pokedex',
      importWorkboxFrom: 'local',
      navigateFallback: 'shell.html'
    },
    appleMobileWebAppStatusBarStyle: '#BC3333'
  },

  pluginOptions: {
    prerenderSpa: {
      registry: undefined,
      renderRoutes: [
        '/',
        '/overview',
        '/about'
      ],
      useRenderEvent: true,
      headless: true,
      onlyProduction: true
    }
  }
}


//configureWebpack (config) => {} (use to add plugins, add things to webpack config with the config variable or just return an object that will be merged

//chainWebpack (config) => {} (see webpack-chain, allows you to change plugin configs, hook in plugins and change it)
