import { createLocalVue, mount } from '@vue/test-utils'
import Pokesearch from '@/components/Pokesearch.vue'
import PokemonList from '../_testData/pokemonList'
import StoreGetters from '../../src/store-parts/store-getters'
import Vuex from 'vuex'
import Vuetify from 'vuetify'

const localVue = createLocalVue();
localVue.use(Vuex)
localVue.use(Vuetify);

describe('Pokesearch.vue', () => {
  let store
  let state
  let actions

  const searchInputAnchor = '[data-test="search-input-field"]';
  const headlineAnchor = '[data-test="headline"]';
  const searchBoxAnchor = '[data-test="search-box"]';

  const shouldBeTitle = "Find your Pokemon"
  const factory = (values = {}) => mount(Pokesearch, {store, localVue, stubs: ['router-link', 'router-view']});

  beforeEach(() => {
    state = {
      pokemonList: PokemonList,
      currentPokemon: {}
    }

    store = new Vuex.Store({
      state,
      actions,
      getters: StoreGetters,
    })
  })

  it('should set up correctly', () => {
    const wrapper = factory();
      expect(wrapper.find(headlineAnchor).text()).toMatch(shouldBeTitle);
      expect(wrapper.find(searchInputAnchor).exists()).toBeTruthy();
  })

  it('should insert search text', () => {
    const wrapper = factory();
    const inputField = wrapper.find(searchInputAnchor);
    const inputText = "Bulbasaur"

    inputField.setValue(inputText);

    expect(wrapper.vm.searchText).toBe(inputText);
    expect(inputField.element.value).toBe(inputText);
  })

  it('should display search result on pokemon name insertion', () => {
    const wrapper = factory();
    const inputText = "Pid";

    wrapper.setData({
      searchText: inputText
    });

    expect(wrapper.vm.pokemonSearchResult).toHaveLength(4);
    expect(wrapper.findAll(searchBoxAnchor + ' div[role=listitem').length).toBe(4);
  })

  it('should display search result on pokemon number insertion', () => {
    const wrapper = factory();
    const inputText = "012";

    wrapper.setData({
      searchText: inputText
    });

    expect(wrapper.vm.pokemonSearchResult).toHaveLength(1);
    expect(wrapper.findAll(searchBoxAnchor + ' div[role=listitem').length).toBe(1);
  });

  it('should clear search text on click on clear icon', () => {
    const wrapper = factory();
    const inputField = wrapper.find(searchInputAnchor);
    const inputText = "Pid";

    wrapper.setData({
      searchText: inputText
    });

    expect(wrapper.vm.searchText).toBe(inputText);
    expect(inputField.element.value).toBe(inputText);

    wrapper.find('.search-input .v-input__icon--clear .v-icon--link').trigger('click');

    expect(wrapper.vm.searchText).toBe(null);
    expect(inputField.element.value).toBe('');
  })
})
