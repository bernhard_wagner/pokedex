import { createLocalVue, mount, shallowMount } from '@vue/test-utils'
import PokemonTypeFilter from '../../src/components/PokemonTypeFilter'
import PokemonTypes from '../../src/const/pokemonTypeColors'
import Vuetify from 'vuetify'


const localVue = createLocalVue();
localVue.use(Vuetify);

describe('PokemonTypeFilter.vue', () => {
  const factory = (values = {}) => mount(PokemonTypeFilter, {localVue});

  const headlineAnchor = '[data-test="headline"]';
  const filterChipsAnchor = '[data-test="filter-chip"]';
  const panelAnchor = '[data-test="panel"]';

  const shouldBeTitle = "Filter by Types"

  it('should set up correctly', () => {
    const wrapper = factory();
    expect(wrapper.find(headlineAnchor).text()).toMatch(shouldBeTitle);
    expect(wrapper.findAll(filterChipsAnchor).length).toBe(Object.keys(PokemonTypes).length);
  })

  it('should set filtered types correctly', () => {
    const wrapper = factory();
    const filterChips = wrapper.findAll(filterChipsAnchor);
    const firstfilterChipElement = filterChips.at(0);
    const secondfilterChipElement = filterChips.at(1);
    const lastfilterChipElement = filterChips.at(filterChips.length - 1);

    expect(wrapper.vm.filteredTypes).toEqual([]);

    expect(firstfilterChipElement.classes()).toContain('selected')
    firstfilterChipElement.element.click();
    expect(firstfilterChipElement.classes()).not.toContain('selected')
    expect(wrapper.vm.filteredTypes).toEqual([firstfilterChipElement.text().toLowerCase()]);

    secondfilterChipElement.element.click();
    expect(wrapper.vm.filteredTypes).toEqual([firstfilterChipElement.text().toLowerCase(), secondfilterChipElement.text().toLowerCase()]);

    firstfilterChipElement.element.click();
    expect(wrapper.vm.filteredTypes).toEqual([secondfilterChipElement.text().toLowerCase()]);

    lastfilterChipElement.element.click();
    expect(wrapper.vm.filteredTypes).toEqual([secondfilterChipElement.text().toLowerCase(), lastfilterChipElement.text().toLowerCase()]);
  })

  it('should emit the onFilter event correctly', () => {
    const wrapper = factory();
    const filterChips = wrapper.findAll(filterChipsAnchor);
    const firstfilterChipElement = filterChips.at(0);
    const lastfilterChipElement = filterChips.at(filterChips.length - 1);

    const shouldBeEvents =  [
      [ [ 'bug' ] ],
      [ [ 'bug', 'water' ] ],
      [ [ 'water' ] ],
      [ [] ]
    ];

    firstfilterChipElement.element.click();
    lastfilterChipElement.element.click();
    firstfilterChipElement.element.click();
    lastfilterChipElement.element.click();

    expect(wrapper.emitted().onFilter.length).toBe(4);
    expect(wrapper.emitted().onFilter).toEqual(shouldBeEvents);
  })

  it('should open and close panel on click', () => {
    const wrapper = factory();
    const panelElement = wrapper.find(panelAnchor);
    const headlineElement = panelElement.find(headlineAnchor);
    const panelBodyElement = panelElement.find('.v-expansion-panel__body');

    wrapper.setData({panel: [false]});
    expect(panelBodyElement.element.style.display).toBe('none');

    headlineElement.element.click();
    expect(wrapper.vm.panel).toEqual([true]);
    expect(panelBodyElement.element.offsetParent).toBe(null);

    headlineElement.element.click();
    expect(wrapper.vm.panel).toEqual([false]);
    expect(panelBodyElement.element.style.display).toBe('none');

  })
})
