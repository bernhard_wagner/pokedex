// -- home -> search string (result check) -> detail view -> ...
module.exports = {
  '@tags': ['test1'],
  'SerachAndDetail 1: navigate to pokedex': browser => {
    browser
      .url('https://pokedex-example.netlify.com/')
      .assert.elementPresent('Body')
      .waitForElementVisible('#app', 5000)
  },
  'SerachAndDetail 2: search for pokemon': browser => {
    browser
      .waitForElementVisible('input[aria-label="Search"]', 5000)
      .assert.attributeEquals('input[aria-label="Search"]', 'Placeholder', '\"Bulbasaur\" or \"001\"')
      .click('input[aria-label="Search"]')
      .setValue('input[aria-label="Search"]', 'Abra')
      .waitForElementVisible('a[href="/pokemon/Abra"]', 5000)
      .pause(5000)
  },
  'SerachAndDetail 3: detail view': browser => {
    browser
    .click('a[href="/pokemon/Abra"]')
    .waitForElementVisible('div[aria-label="Kadabra"]', 5000)
    .pause(5000)
    .end()
  }
}
