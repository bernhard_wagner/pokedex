export default [{ 'id': 'UG9rZW1vbjowMDE=', 'number': '001', 'name': 'Bulbasaur', 'image': 'https://img.pokemondb.net/artwork/bulbasaur.jpg', 'types': ['Grass', 'Poison'], '__typename': 'Pokemon' }, {
  'id': 'UG9rZW1vbjowMDI=',
  'number': '002',
  'name': 'Ivysaur',
  'image': 'https://img.pokemondb.net/artwork/ivysaur.jpg',
  'types': ['Grass', 'Poison'],
  '__typename': 'Pokemon'
}, { 'id': 'UG9rZW1vbjowMDM=', 'number': '003', 'name': 'Venusaur', 'image': 'https://img.pokemondb.net/artwork/venusaur.jpg', 'types': ['Grass', 'Poison'], '__typename': 'Pokemon' }, {
  'id': 'UG9rZW1vbjowMDQ=',
  'number': '004',
  'name': 'Charmander',
  'image': 'https://img.pokemondb.net/artwork/charmander.jpg',
  'types': ['Fire'],
  '__typename': 'Pokemon'
}, { 'id': 'UG9rZW1vbjowMDU=', 'number': '005', 'name': 'Charmeleon', 'image': 'https://img.pokemondb.net/artwork/charmeleon.jpg', 'types': ['Fire'], '__typename': 'Pokemon' }, {
  'id': 'UG9rZW1vbjowMDY=',
  'number': '006',
  'name': 'Charizard',
  'image': 'https://img.pokemondb.net/artwork/charizard.jpg',
  'types': ['Fire', 'Flying'],
  '__typename': 'Pokemon'
}, { 'id': 'UG9rZW1vbjowMDc=', 'number': '007', 'name': 'Squirtle', 'image': 'https://img.pokemondb.net/artwork/squirtle.jpg', 'types': ['Water'], '__typename': 'Pokemon' }, {
  'id': 'UG9rZW1vbjowMDg=',
  'number': '008',
  'name': 'Wartortle',
  'image': 'https://img.pokemondb.net/artwork/wartortle.jpg',
  'types': ['Water'],
  '__typename': 'Pokemon'
}, { 'id': 'UG9rZW1vbjowMDk=', 'number': '009', 'name': 'Blastoise', 'image': 'https://img.pokemondb.net/artwork/blastoise.jpg', 'types': ['Water'], '__typename': 'Pokemon' }, {
  'id': 'UG9rZW1vbjowMTA=',
  'number': '010',
  'name': 'Caterpie',
  'image': 'https://img.pokemondb.net/artwork/caterpie.jpg',
  'types': ['Bug'],
  '__typename': 'Pokemon'
}, { 'id': 'UG9rZW1vbjowMTE=', 'number': '011', 'name': 'Metapod', 'image': 'https://img.pokemondb.net/artwork/metapod.jpg', 'types': ['Bug'], '__typename': 'Pokemon' }, {
  'id': 'UG9rZW1vbjowMTI=',
  'number': '012',
  'name': 'Butterfree',
  'image': 'https://img.pokemondb.net/artwork/butterfree.jpg',
  'types': ['Bug', 'Flying'],
  '__typename': 'Pokemon'
}, { 'id': 'UG9rZW1vbjowMTM=', 'number': '013', 'name': 'Weedle', 'image': 'https://img.pokemondb.net/artwork/weedle.jpg', 'types': ['Bug', 'Poison'], '__typename': 'Pokemon' }, {
  'id': 'UG9rZW1vbjowMTQ=',
  'number': '014',
  'name': 'Kakuna',
  'image': 'https://img.pokemondb.net/artwork/kakuna.jpg',
  'types': ['Bug', 'Poison'],
  '__typename': 'Pokemon'
}, { 'id': 'UG9rZW1vbjowMTU=', 'number': '015', 'name': 'Beedrill', 'image': 'https://img.pokemondb.net/artwork/beedrill.jpg', 'types': ['Bug', 'Poison'], '__typename': 'Pokemon' }, {
  'id': 'UG9rZW1vbjowMTY=',
  'number': '016',
  'name': 'Pidgey',
  'image': 'https://img.pokemondb.net/artwork/pidgey.jpg',
  'types': ['Normal', 'Flying'],
  '__typename': 'Pokemon'
}, { 'id': 'UG9rZW1vbjowMTc=', 'number': '017', 'name': 'Pidgeotto', 'image': 'https://img.pokemondb.net/artwork/pidgeotto.jpg', 'types': ['Normal', 'Flying'], '__typename': 'Pokemon' }, {
  'id': 'UG9rZW1vbjowMTg=',
  'number': '018',
  'name': 'Pidgeot',
  'image': 'https://img.pokemondb.net/artwork/pidgeot.jpg',
  'types': ['Normal', 'Flying'],
  '__typename': 'Pokemon'
}, { 'id': 'UG9rZW1vbjowMTk=', 'number': '019', 'name': 'Rattata', 'image': 'https://img.pokemondb.net/artwork/rattata.jpg', 'types': ['Normal'], '__typename': 'Pokemon' }, {
  'id': 'UG9rZW1vbjowMjA=',
  'number': '020',
  'name': 'Raticate',
  'image': 'https://img.pokemondb.net/artwork/raticate.jpg',
  'types': ['Normal'],
  '__typename': 'Pokemon'
}, { 'id': 'UG9rZW1vbjowMjE=', 'number': '021', 'name': 'Spearow', 'image': 'https://img.pokemondb.net/artwork/spearow.jpg', 'types': ['Normal', 'Flying'], '__typename': 'Pokemon' }, {
  'id': 'UG9rZW1vbjowMjI=',
  'number': '022',
  'name': 'Fearow',
  'image': 'https://img.pokemondb.net/artwork/fearow.jpg',
  'types': ['Normal', 'Flying'],
  '__typename': 'Pokemon'
}, { 'id': 'UG9rZW1vbjowMjM=', 'number': '023', 'name': 'Ekans', 'image': 'https://img.pokemondb.net/artwork/ekans.jpg', 'types': ['Poison'], '__typename': 'Pokemon' }, {
  'id': 'UG9rZW1vbjowMjQ=',
  'number': '024',
  'name': 'Arbok',
  'image': 'https://img.pokemondb.net/artwork/arbok.jpg',
  'types': ['Poison'],
  '__typename': 'Pokemon'
}, { 'id': 'UG9rZW1vbjowMjU=', 'number': '025', 'name': 'Pikachu', 'image': 'https://img.pokemondb.net/artwork/pikachu.jpg', 'types': ['Electric'], '__typename': 'Pokemon' }, {
  'id': 'UG9rZW1vbjowMjY=',
  'number': '026',
  'name': 'Raichu',
  'image': 'https://img.pokemondb.net/artwork/raichu.jpg',
  'types': ['Electric'],
  '__typename': 'Pokemon'
}, { 'id': 'UG9rZW1vbjowMjc=', 'number': '027', 'name': 'Sandshrew', 'image': 'https://img.pokemondb.net/artwork/sandshrew.jpg', 'types': ['Ground'], '__typename': 'Pokemon' }, {
  'id': 'UG9rZW1vbjowMjg=',
  'number': '028',
  'name': 'Sandslash',
  'image': 'https://img.pokemondb.net/artwork/sandslash.jpg',
  'types': ['Ground'],
  '__typename': 'Pokemon'
}, { 'id': 'UG9rZW1vbjowMjk=', 'number': '029', 'name': 'Nidoran-F', 'image': 'https://img.pokemondb.net/artwork/nidoran-f.jpg', 'types': ['Poison'], '__typename': 'Pokemon' }, {
  'id': 'UG9rZW1vbjowMzA=',
  'number': '030',
  'name': 'Nidorina',
  'image': 'https://img.pokemondb.net/artwork/nidorina.jpg',
  'types': ['Poison'],
  '__typename': 'Pokemon'
}, { 'id': 'UG9rZW1vbjowMzE=', 'number': '031', 'name': 'Nidoqueen', 'image': 'https://img.pokemondb.net/artwork/nidoqueen.jpg', 'types': ['Poison', 'Ground'], '__typename': 'Pokemon' }, {
  'id': 'UG9rZW1vbjowMzI=',
  'number': '032',
  'name': 'Nidoran-M',
  'image': 'https://img.pokemondb.net/artwork/nidoran-m.jpg',
  'types': ['Poison'],
  '__typename': 'Pokemon'
}, { 'id': 'UG9rZW1vbjowMzM=', 'number': '033', 'name': 'Nidorino', 'image': 'https://img.pokemondb.net/artwork/nidorino.jpg', 'types': ['Poison'], '__typename': 'Pokemon' }, {
  'id': 'UG9rZW1vbjowMzQ=',
  'number': '034',
  'name': 'Nidoking',
  'image': 'https://img.pokemondb.net/artwork/nidoking.jpg',
  'types': ['Poison', 'Ground'],
  '__typename': 'Pokemon'
}, { 'id': 'UG9rZW1vbjowMzU=', 'number': '035', 'name': 'Clefairy', 'image': 'https://img.pokemondb.net/artwork/clefairy.jpg', 'types': ['Fairy'], '__typename': 'Pokemon' }, {
  'id': 'UG9rZW1vbjowMzY=',
  'number': '036',
  'name': 'Clefable',
  'image': 'https://img.pokemondb.net/artwork/clefable.jpg',
  'types': ['Fairy'],
  '__typename': 'Pokemon'
}, { 'id': 'UG9rZW1vbjowMzc=', 'number': '037', 'name': 'Vulpix', 'image': 'https://img.pokemondb.net/artwork/vulpix.jpg', 'types': ['Fire'], '__typename': 'Pokemon' }, {
  'id': 'UG9rZW1vbjowMzg=',
  'number': '038',
  'name': 'Ninetales',
  'image': 'https://img.pokemondb.net/artwork/ninetales.jpg',
  'types': ['Fire'],
  '__typename': 'Pokemon'
}, { 'id': 'UG9rZW1vbjowMzk=', 'number': '039', 'name': 'Jigglypuff', 'image': 'https://img.pokemondb.net/artwork/jigglypuff.jpg', 'types': ['Normal', 'Fairy'], '__typename': 'Pokemon' }, {
  'id': 'UG9rZW1vbjowNDA=',
  'number': '040',
  'name': 'Wigglytuff',
  'image': 'https://img.pokemondb.net/artwork/wigglytuff.jpg',
  'types': ['Normal', 'Fairy'],
  '__typename': 'Pokemon'
}, { 'id': 'UG9rZW1vbjowNDE=', 'number': '041', 'name': 'Zubat', 'image': 'https://img.pokemondb.net/artwork/zubat.jpg', 'types': ['Poison', 'Flying'], '__typename': 'Pokemon' }, {
  'id': 'UG9rZW1vbjowNDI=',
  'number': '042',
  'name': 'Golbat',
  'image': 'https://img.pokemondb.net/artwork/golbat.jpg',
  'types': ['Poison', 'Flying'],
  '__typename': 'Pokemon'
}, { 'id': 'UG9rZW1vbjowNDM=', 'number': '043', 'name': 'Oddish', 'image': 'https://img.pokemondb.net/artwork/oddish.jpg', 'types': ['Grass', 'Poison'], '__typename': 'Pokemon' }, {
  'id': 'UG9rZW1vbjowNDQ=',
  'number': '044',
  'name': 'Gloom',
  'image': 'https://img.pokemondb.net/artwork/gloom.jpg',
  'types': ['Grass', 'Poison'],
  '__typename': 'Pokemon'
}, { 'id': 'UG9rZW1vbjowNDU=', 'number': '045', 'name': 'Vileplume', 'image': 'https://img.pokemondb.net/artwork/vileplume.jpg', 'types': ['Grass', 'Poison'], '__typename': 'Pokemon' }, {
  'id': 'UG9rZW1vbjowNDY=',
  'number': '046',
  'name': 'Paras',
  'image': 'https://img.pokemondb.net/artwork/paras.jpg',
  'types': ['Bug', 'Grass'],
  '__typename': 'Pokemon'
}, { 'id': 'UG9rZW1vbjowNDc=', 'number': '047', 'name': 'Parasect', 'image': 'https://img.pokemondb.net/artwork/parasect.jpg', 'types': ['Bug', 'Grass'], '__typename': 'Pokemon' }, {
  'id': 'UG9rZW1vbjowNDg=',
  'number': '048',
  'name': 'Venonat',
  'image': 'https://img.pokemondb.net/artwork/venonat.jpg',
  'types': ['Bug', 'Poison'],
  '__typename': 'Pokemon'
}, { 'id': 'UG9rZW1vbjowNDk=', 'number': '049', 'name': 'Venomoth', 'image': 'https://img.pokemondb.net/artwork/venomoth.jpg', 'types': ['Bug', 'Poison'], '__typename': 'Pokemon' }, {
  'id': 'UG9rZW1vbjowNTA=',
  'number': '050',
  'name': 'Diglett',
  'image': 'https://img.pokemondb.net/artwork/diglett.jpg',
  'types': ['Ground'],
  '__typename': 'Pokemon'
}, { 'id': 'UG9rZW1vbjowNTE=', 'number': '051', 'name': 'Dugtrio', 'image': 'https://img.pokemondb.net/artwork/dugtrio.jpg', 'types': ['Ground'], '__typename': 'Pokemon' }, {
  'id': 'UG9rZW1vbjowNTI=',
  'number': '052',
  'name': 'Meowth',
  'image': 'https://img.pokemondb.net/artwork/meowth.jpg',
  'types': ['Normal'],
  '__typename': 'Pokemon'
}, { 'id': 'UG9rZW1vbjowNTM=', 'number': '053', 'name': 'Persian', 'image': 'https://img.pokemondb.net/artwork/persian.jpg', 'types': ['Normal'], '__typename': 'Pokemon' }, {
  'id': 'UG9rZW1vbjowNTQ=',
  'number': '054',
  'name': 'Psyduck',
  'image': 'https://img.pokemondb.net/artwork/psyduck.jpg',
  'types': ['Water'],
  '__typename': 'Pokemon'
}, { 'id': 'UG9rZW1vbjowNTU=', 'number': '055', 'name': 'Golduck', 'image': 'https://img.pokemondb.net/artwork/golduck.jpg', 'types': ['Water'], '__typename': 'Pokemon' }, {
  'id': 'UG9rZW1vbjowNTY=',
  'number': '056',
  'name': 'Mankey',
  'image': 'https://img.pokemondb.net/artwork/mankey.jpg',
  'types': ['Fighting'],
  '__typename': 'Pokemon'
}, { 'id': 'UG9rZW1vbjowNTc=', 'number': '057', 'name': 'Primeape', 'image': 'https://img.pokemondb.net/artwork/primeape.jpg', 'types': ['Fighting'], '__typename': 'Pokemon' }, {
  'id': 'UG9rZW1vbjowNTg=',
  'number': '058',
  'name': 'Growlithe',
  'image': 'https://img.pokemondb.net/artwork/growlithe.jpg',
  'types': ['Fire'],
  '__typename': 'Pokemon'
}, { 'id': 'UG9rZW1vbjowNTk=', 'number': '059', 'name': 'Arcanine', 'image': 'https://img.pokemondb.net/artwork/arcanine.jpg', 'types': ['Fire'], '__typename': 'Pokemon' }, {
  'id': 'UG9rZW1vbjowNjA=',
  'number': '060',
  'name': 'Poliwag',
  'image': 'https://img.pokemondb.net/artwork/poliwag.jpg',
  'types': ['Water'],
  '__typename': 'Pokemon'
}, { 'id': 'UG9rZW1vbjowNjE=', 'number': '061', 'name': 'Poliwhirl', 'image': 'https://img.pokemondb.net/artwork/poliwhirl.jpg', 'types': ['Water'], '__typename': 'Pokemon' }, {
  'id': 'UG9rZW1vbjowNjI=',
  'number': '062',
  'name': 'Poliwrath',
  'image': 'https://img.pokemondb.net/artwork/poliwrath.jpg',
  'types': ['Water', 'Fighting'],
  '__typename': 'Pokemon'
}, { 'id': 'UG9rZW1vbjowNjM=', 'number': '063', 'name': 'Abra', 'image': 'https://img.pokemondb.net/artwork/abra.jpg', 'types': ['Psychic'], '__typename': 'Pokemon' }, {
  'id': 'UG9rZW1vbjowNjQ=',
  'number': '064',
  'name': 'Kadabra',
  'image': 'https://img.pokemondb.net/artwork/kadabra.jpg',
  'types': ['Psychic'],
  '__typename': 'Pokemon'
}, { 'id': 'UG9rZW1vbjowNjU=', 'number': '065', 'name': 'Alakazam', 'image': 'https://img.pokemondb.net/artwork/alakazam.jpg', 'types': ['Psychic'], '__typename': 'Pokemon' }, {
  'id': 'UG9rZW1vbjowNjY=',
  'number': '066',
  'name': 'Machop',
  'image': 'https://img.pokemondb.net/artwork/machop.jpg',
  'types': ['Fighting'],
  '__typename': 'Pokemon'
}, { 'id': 'UG9rZW1vbjowNjc=', 'number': '067', 'name': 'Machoke', 'image': 'https://img.pokemondb.net/artwork/machoke.jpg', 'types': ['Fighting'], '__typename': 'Pokemon' }, {
  'id': 'UG9rZW1vbjowNjg=',
  'number': '068',
  'name': 'Machamp',
  'image': 'https://img.pokemondb.net/artwork/machamp.jpg',
  'types': ['Fighting'],
  '__typename': 'Pokemon'
}, { 'id': 'UG9rZW1vbjowNjk=', 'number': '069', 'name': 'Bellsprout', 'image': 'https://img.pokemondb.net/artwork/bellsprout.jpg', 'types': ['Grass', 'Poison'], '__typename': 'Pokemon' }, {
  'id': 'UG9rZW1vbjowNzA=',
  'number': '070',
  'name': 'Weepinbell',
  'image': 'https://img.pokemondb.net/artwork/weepinbell.jpg',
  'types': ['Grass', 'Poison'],
  '__typename': 'Pokemon'
}, { 'id': 'UG9rZW1vbjowNzE=', 'number': '071', 'name': 'Victreebel', 'image': 'https://img.pokemondb.net/artwork/victreebel.jpg', 'types': ['Grass', 'Poison'], '__typename': 'Pokemon' }, {
  'id': 'UG9rZW1vbjowNzI=',
  'number': '072',
  'name': 'Tentacool',
  'image': 'https://img.pokemondb.net/artwork/tentacool.jpg',
  'types': ['Water', 'Poison'],
  '__typename': 'Pokemon'
}, { 'id': 'UG9rZW1vbjowNzM=', 'number': '073', 'name': 'Tentacruel', 'image': 'https://img.pokemondb.net/artwork/tentacruel.jpg', 'types': ['Water', 'Poison'], '__typename': 'Pokemon' }, {
  'id': 'UG9rZW1vbjowNzQ=',
  'number': '074',
  'name': 'Geodude',
  'image': 'https://img.pokemondb.net/artwork/geodude.jpg',
  'types': ['Rock', 'Ground'],
  '__typename': 'Pokemon'
}, { 'id': 'UG9rZW1vbjowNzU=', 'number': '075', 'name': 'Graveler', 'image': 'https://img.pokemondb.net/artwork/graveler.jpg', 'types': ['Rock', 'Ground'], '__typename': 'Pokemon' }, {
  'id': 'UG9rZW1vbjowNzY=',
  'number': '076',
  'name': 'Golem',
  'image': 'https://img.pokemondb.net/artwork/golem.jpg',
  'types': ['Rock', 'Ground'],
  '__typename': 'Pokemon'
}, { 'id': 'UG9rZW1vbjowNzc=', 'number': '077', 'name': 'Ponyta', 'image': 'https://img.pokemondb.net/artwork/ponyta.jpg', 'types': ['Fire'], '__typename': 'Pokemon' }, {
  'id': 'UG9rZW1vbjowNzg=',
  'number': '078',
  'name': 'Rapidash',
  'image': 'https://img.pokemondb.net/artwork/rapidash.jpg',
  'types': ['Fire'],
  '__typename': 'Pokemon'
}, { 'id': 'UG9rZW1vbjowNzk=', 'number': '079', 'name': 'Slowpoke', 'image': 'https://img.pokemondb.net/artwork/slowpoke.jpg', 'types': ['Water', 'Psychic'], '__typename': 'Pokemon' }, {
  'id': 'UG9rZW1vbjowODA=',
  'number': '080',
  'name': 'Slowbro',
  'image': 'https://img.pokemondb.net/artwork/slowbro.jpg',
  'types': ['Water', 'Psychic'],
  '__typename': 'Pokemon'
}, { 'id': 'UG9rZW1vbjowODE=', 'number': '081', 'name': 'Magnemite', 'image': 'https://img.pokemondb.net/artwork/magnemite.jpg', 'types': ['Electric', 'Steel'], '__typename': 'Pokemon' }, {
  'id': 'UG9rZW1vbjowODI=',
  'number': '082',
  'name': 'Magneton',
  'image': 'https://img.pokemondb.net/artwork/magneton.jpg',
  'types': ['Electric', 'Steel'],
  '__typename': 'Pokemon'
}, { 'id': 'UG9rZW1vbjowODM=', 'number': '083', 'name': 'Farfetch\'d', 'image': 'https://img.pokemondb.net/artwork/farfetchd.jpg', 'types': ['Normal', 'Flying'], '__typename': 'Pokemon' }, {
  'id': 'UG9rZW1vbjowODQ=',
  'number': '084',
  'name': 'Doduo',
  'image': 'https://img.pokemondb.net/artwork/doduo.jpg',
  'types': ['Normal', 'Flying'],
  '__typename': 'Pokemon'
}, { 'id': 'UG9rZW1vbjowODU=', 'number': '085', 'name': 'Dodrio', 'image': 'https://img.pokemondb.net/artwork/dodrio.jpg', 'types': ['Normal', 'Flying'], '__typename': 'Pokemon' }, {
  'id': 'UG9rZW1vbjowODY=',
  'number': '086',
  'name': 'Seel',
  'image': 'https://img.pokemondb.net/artwork/seel.jpg',
  'types': ['Water'],
  '__typename': 'Pokemon'
}, { 'id': 'UG9rZW1vbjowODc=', 'number': '087', 'name': 'Dewgong', 'image': 'https://img.pokemondb.net/artwork/dewgong.jpg', 'types': ['Water', 'Ice'], '__typename': 'Pokemon' }, {
  'id': 'UG9rZW1vbjowODg=',
  'number': '088',
  'name': 'Grimer',
  'image': 'https://img.pokemondb.net/artwork/grimer.jpg',
  'types': ['Poison'],
  '__typename': 'Pokemon'
}, { 'id': 'UG9rZW1vbjowODk=', 'number': '089', 'name': 'Muk', 'image': 'https://img.pokemondb.net/artwork/muk.jpg', 'types': ['Poison'], '__typename': 'Pokemon' }, {
  'id': 'UG9rZW1vbjowOTA=',
  'number': '090',
  'name': 'Shellder',
  'image': 'https://img.pokemondb.net/artwork/shellder.jpg',
  'types': ['Water'],
  '__typename': 'Pokemon'
}, { 'id': 'UG9rZW1vbjowOTE=', 'number': '091', 'name': 'Cloyster', 'image': 'https://img.pokemondb.net/artwork/cloyster.jpg', 'types': ['Water', 'Ice'], '__typename': 'Pokemon' }, {
  'id': 'UG9rZW1vbjowOTI=',
  'number': '092',
  'name': 'Gastly',
  'image': 'https://img.pokemondb.net/artwork/gastly.jpg',
  'types': ['Ghost', 'Poison'],
  '__typename': 'Pokemon'
}, { 'id': 'UG9rZW1vbjowOTM=', 'number': '093', 'name': 'Haunter', 'image': 'https://img.pokemondb.net/artwork/haunter.jpg', 'types': ['Ghost', 'Poison'], '__typename': 'Pokemon' }, {
  'id': 'UG9rZW1vbjowOTQ=',
  'number': '094',
  'name': 'Gengar',
  'image': 'https://img.pokemondb.net/artwork/gengar.jpg',
  'types': ['Ghost', 'Poison'],
  '__typename': 'Pokemon'
}, { 'id': 'UG9rZW1vbjowOTU=', 'number': '095', 'name': 'Onix', 'image': 'https://img.pokemondb.net/artwork/onix.jpg', 'types': ['Rock', 'Ground'], '__typename': 'Pokemon' }, {
  'id': 'UG9rZW1vbjowOTY=',
  'number': '096',
  'name': 'Drowzee',
  'image': 'https://img.pokemondb.net/artwork/drowzee.jpg',
  'types': ['Psychic'],
  '__typename': 'Pokemon'
}, { 'id': 'UG9rZW1vbjowOTc=', 'number': '097', 'name': 'Hypno', 'image': 'https://img.pokemondb.net/artwork/hypno.jpg', 'types': ['Psychic'], '__typename': 'Pokemon' }, {
  'id': 'UG9rZW1vbjowOTg=',
  'number': '098',
  'name': 'Krabby',
  'image': 'https://img.pokemondb.net/artwork/krabby.jpg',
  'types': ['Water'],
  '__typename': 'Pokemon'
}, { 'id': 'UG9rZW1vbjowOTk=', 'number': '099', 'name': 'Kingler', 'image': 'https://img.pokemondb.net/artwork/kingler.jpg', 'types': ['Water'], '__typename': 'Pokemon' }, {
  'id': 'UG9rZW1vbjoxMDA=',
  'number': '100',
  'name': 'Voltorb',
  'image': 'https://img.pokemondb.net/artwork/voltorb.jpg',
  'types': ['Electric'],
  '__typename': 'Pokemon'
}, { 'id': 'UG9rZW1vbjoxMDE=', 'number': '101', 'name': 'Electrode', 'image': 'https://img.pokemondb.net/artwork/electrode.jpg', 'types': ['Electric'], '__typename': 'Pokemon' }, {
  'id': 'UG9rZW1vbjoxMDI=',
  'number': '102',
  'name': 'Exeggcute',
  'image': 'https://img.pokemondb.net/artwork/exeggcute.jpg',
  'types': ['Grass', 'Psychic'],
  '__typename': 'Pokemon'
}, { 'id': 'UG9rZW1vbjoxMDM=', 'number': '103', 'name': 'Exeggutor', 'image': 'https://img.pokemondb.net/artwork/exeggutor.jpg', 'types': ['Grass', 'Psychic'], '__typename': 'Pokemon' }, {
  'id': 'UG9rZW1vbjoxMDQ=',
  'number': '104',
  'name': 'Cubone',
  'image': 'https://img.pokemondb.net/artwork/cubone.jpg',
  'types': ['Ground'],
  '__typename': 'Pokemon'
}, { 'id': 'UG9rZW1vbjoxMDU=', 'number': '105', 'name': 'Marowak', 'image': 'https://img.pokemondb.net/artwork/marowak.jpg', 'types': ['Ground'], '__typename': 'Pokemon' }, {
  'id': 'UG9rZW1vbjoxMDY=',
  'number': '106',
  'name': 'Hitmonlee',
  'image': 'https://img.pokemondb.net/artwork/hitmonlee.jpg',
  'types': ['Fighting'],
  '__typename': 'Pokemon'
}, { 'id': 'UG9rZW1vbjoxMDc=', 'number': '107', 'name': 'Hitmonchan', 'image': 'https://img.pokemondb.net/artwork/hitmonchan.jpg', 'types': ['Fighting'], '__typename': 'Pokemon' }, {
  'id': 'UG9rZW1vbjoxMDg=',
  'number': '108',
  'name': 'Lickitung',
  'image': 'https://img.pokemondb.net/artwork/lickitung.jpg',
  'types': ['Normal'],
  '__typename': 'Pokemon'
}, { 'id': 'UG9rZW1vbjoxMDk=', 'number': '109', 'name': 'Koffing', 'image': 'https://img.pokemondb.net/artwork/koffing.jpg', 'types': ['Poison'], '__typename': 'Pokemon' }, {
  'id': 'UG9rZW1vbjoxMTA=',
  'number': '110',
  'name': 'Weezing',
  'image': 'https://img.pokemondb.net/artwork/weezing.jpg',
  'types': ['Poison'],
  '__typename': 'Pokemon'
}, { 'id': 'UG9rZW1vbjoxMTE=', 'number': '111', 'name': 'Rhyhorn', 'image': 'https://img.pokemondb.net/artwork/rhyhorn.jpg', 'types': ['Ground', 'Rock'], '__typename': 'Pokemon' }, {
  'id': 'UG9rZW1vbjoxMTI=',
  'number': '112',
  'name': 'Rhydon',
  'image': 'https://img.pokemondb.net/artwork/rhydon.jpg',
  'types': ['Ground', 'Rock'],
  '__typename': 'Pokemon'
}, { 'id': 'UG9rZW1vbjoxMTM=', 'number': '113', 'name': 'Chansey', 'image': 'https://img.pokemondb.net/artwork/chansey.jpg', 'types': ['Normal'], '__typename': 'Pokemon' }, {
  'id': 'UG9rZW1vbjoxMTQ=',
  'number': '114',
  'name': 'Tangela',
  'image': 'https://img.pokemondb.net/artwork/tangela.jpg',
  'types': ['Grass'],
  '__typename': 'Pokemon'
}, { 'id': 'UG9rZW1vbjoxMTU=', 'number': '115', 'name': 'Kangaskhan', 'image': 'https://img.pokemondb.net/artwork/kangaskhan.jpg', 'types': ['Normal'], '__typename': 'Pokemon' }, {
  'id': 'UG9rZW1vbjoxMTY=',
  'number': '116',
  'name': 'Horsea',
  'image': 'https://img.pokemondb.net/artwork/horsea.jpg',
  'types': ['Water'],
  '__typename': 'Pokemon'
}, { 'id': 'UG9rZW1vbjoxMTc=', 'number': '117', 'name': 'Seadra', 'image': 'https://img.pokemondb.net/artwork/seadra.jpg', 'types': ['Water'], '__typename': 'Pokemon' }, {
  'id': 'UG9rZW1vbjoxMTg=',
  'number': '118',
  'name': 'Goldeen',
  'image': 'https://img.pokemondb.net/artwork/goldeen.jpg',
  'types': ['Water'],
  '__typename': 'Pokemon'
}, { 'id': 'UG9rZW1vbjoxMTk=', 'number': '119', 'name': 'Seaking', 'image': 'https://img.pokemondb.net/artwork/seaking.jpg', 'types': ['Water'], '__typename': 'Pokemon' }, {
  'id': 'UG9rZW1vbjoxMjA=',
  'number': '120',
  'name': 'Staryu',
  'image': 'https://img.pokemondb.net/artwork/staryu.jpg',
  'types': ['Water'],
  '__typename': 'Pokemon'
}, { 'id': 'UG9rZW1vbjoxMjE=', 'number': '121', 'name': 'Starmie', 'image': 'https://img.pokemondb.net/artwork/starmie.jpg', 'types': ['Water', 'Psychic'], '__typename': 'Pokemon' }, {
  'id': 'UG9rZW1vbjoxMjI=',
  'number': '122',
  'name': 'Mr. Mime',
  'image': 'https://img.pokemondb.net/artwork/mr-mime.jpg',
  'types': ['Psychic', 'Fairy'],
  '__typename': 'Pokemon'
}, { 'id': 'UG9rZW1vbjoxMjM=', 'number': '123', 'name': 'Scyther', 'image': 'https://img.pokemondb.net/artwork/scyther.jpg', 'types': ['Bug', 'Flying'], '__typename': 'Pokemon' }, {
  'id': 'UG9rZW1vbjoxMjQ=',
  'number': '124',
  'name': 'Jynx',
  'image': 'https://img.pokemondb.net/artwork/jynx.jpg',
  'types': ['Ice', 'Psychic'],
  '__typename': 'Pokemon'
}, { 'id': 'UG9rZW1vbjoxMjU=', 'number': '125', 'name': 'Electabuzz', 'image': 'https://img.pokemondb.net/artwork/electabuzz.jpg', 'types': ['Electric'], '__typename': 'Pokemon' }, {
  'id': 'UG9rZW1vbjoxMjY=',
  'number': '126',
  'name': 'Magmar',
  'image': 'https://img.pokemondb.net/artwork/magmar.jpg',
  'types': ['Fire'],
  '__typename': 'Pokemon'
}, { 'id': 'UG9rZW1vbjoxMjc=', 'number': '127', 'name': 'Pinsir', 'image': 'https://img.pokemondb.net/artwork/pinsir.jpg', 'types': ['Bug'], '__typename': 'Pokemon' }, {
  'id': 'UG9rZW1vbjoxMjg=',
  'number': '128',
  'name': 'Tauros',
  'image': 'https://img.pokemondb.net/artwork/tauros.jpg',
  'types': ['Normal'],
  '__typename': 'Pokemon'
}, { 'id': 'UG9rZW1vbjoxMjk=', 'number': '129', 'name': 'Magikarp', 'image': 'https://img.pokemondb.net/artwork/magikarp.jpg', 'types': ['Water'], '__typename': 'Pokemon' }, {
  'id': 'UG9rZW1vbjoxMzA=',
  'number': '130',
  'name': 'Gyarados',
  'image': 'https://img.pokemondb.net/artwork/gyarados.jpg',
  'types': ['Water', 'Flying'],
  '__typename': 'Pokemon'
}, { 'id': 'UG9rZW1vbjoxMzE=', 'number': '131', 'name': 'Lapras', 'image': 'https://img.pokemondb.net/artwork/lapras.jpg', 'types': ['Water', 'Ice'], '__typename': 'Pokemon' }, {
  'id': 'UG9rZW1vbjoxMzI=',
  'number': '132',
  'name': 'Ditto',
  'image': 'https://img.pokemondb.net/artwork/ditto.jpg',
  'types': ['Normal'],
  '__typename': 'Pokemon'
}, { 'id': 'UG9rZW1vbjoxMzM=', 'number': '133', 'name': 'Eevee', 'image': 'https://img.pokemondb.net/artwork/eevee.jpg', 'types': ['Normal'], '__typename': 'Pokemon' }, {
  'id': 'UG9rZW1vbjoxMzQ=',
  'number': '134',
  'name': 'Vaporeon',
  'image': 'https://img.pokemondb.net/artwork/vaporeon.jpg',
  'types': ['Water'],
  '__typename': 'Pokemon'
}, { 'id': 'UG9rZW1vbjoxMzU=', 'number': '135', 'name': 'Jolteon', 'image': 'https://img.pokemondb.net/artwork/jolteon.jpg', 'types': ['Electric'], '__typename': 'Pokemon' }, {
  'id': 'UG9rZW1vbjoxMzY=',
  'number': '136',
  'name': 'Flareon',
  'image': 'https://img.pokemondb.net/artwork/flareon.jpg',
  'types': ['Fire'],
  '__typename': 'Pokemon'
}, { 'id': 'UG9rZW1vbjoxMzc=', 'number': '137', 'name': 'Porygon', 'image': 'https://img.pokemondb.net/artwork/porygon.jpg', 'types': ['Normal'], '__typename': 'Pokemon' }, {
  'id': 'UG9rZW1vbjoxMzg=',
  'number': '138',
  'name': 'Omanyte',
  'image': 'https://img.pokemondb.net/artwork/omanyte.jpg',
  'types': ['Rock', 'Water'],
  '__typename': 'Pokemon'
}, { 'id': 'UG9rZW1vbjoxMzk=', 'number': '139', 'name': 'Omastar', 'image': 'https://img.pokemondb.net/artwork/omastar.jpg', 'types': ['Rock', 'Water'], '__typename': 'Pokemon' }, {
  'id': 'UG9rZW1vbjoxNDA=',
  'number': '140',
  'name': 'Kabuto',
  'image': 'https://img.pokemondb.net/artwork/kabuto.jpg',
  'types': ['Rock', 'Water'],
  '__typename': 'Pokemon'
}, { 'id': 'UG9rZW1vbjoxNDE=', 'number': '141', 'name': 'Kabutops', 'image': 'https://img.pokemondb.net/artwork/kabutops.jpg', 'types': ['Rock', 'Water'], '__typename': 'Pokemon' }, {
  'id': 'UG9rZW1vbjoxNDI=',
  'number': '142',
  'name': 'Aerodactyl',
  'image': 'https://img.pokemondb.net/artwork/aerodactyl.jpg',
  'types': ['Rock', 'Flying'],
  '__typename': 'Pokemon'
}, { 'id': 'UG9rZW1vbjoxNDM=', 'number': '143', 'name': 'Snorlax', 'image': 'https://img.pokemondb.net/artwork/snorlax.jpg', 'types': ['Normal'], '__typename': 'Pokemon' }, {
  'id': 'UG9rZW1vbjoxNDQ=',
  'number': '144',
  'name': 'Articuno',
  'image': 'https://img.pokemondb.net/artwork/articuno.jpg',
  'types': ['Ice', 'Flying'],
  '__typename': 'Pokemon'
}, { 'id': 'UG9rZW1vbjoxNDU=', 'number': '145', 'name': 'Zapdos', 'image': 'https://img.pokemondb.net/artwork/zapdos.jpg', 'types': ['Electric', 'Flying'], '__typename': 'Pokemon' }, {
  'id': 'UG9rZW1vbjoxNDY=',
  'number': '146',
  'name': 'Moltres',
  'image': 'https://img.pokemondb.net/artwork/moltres.jpg',
  'types': ['Fire', 'Flying'],
  '__typename': 'Pokemon'
}, { 'id': 'UG9rZW1vbjoxNDc=', 'number': '147', 'name': 'Dratini', 'image': 'https://img.pokemondb.net/artwork/dratini.jpg', 'types': ['Dragon'], '__typename': 'Pokemon' }, {
  'id': 'UG9rZW1vbjoxNDg=',
  'number': '148',
  'name': 'Dragonair',
  'image': 'https://img.pokemondb.net/artwork/dragonair.jpg',
  'types': ['Dragon'],
  '__typename': 'Pokemon'
}, { 'id': 'UG9rZW1vbjoxNDk=', 'number': '149', 'name': 'Dragonite', 'image': 'https://img.pokemondb.net/artwork/dragonite.jpg', 'types': ['Dragon', 'Flying'], '__typename': 'Pokemon' }, {
  'id': 'UG9rZW1vbjoxNTA=',
  'number': '150',
  'name': 'Mewtwo',
  'image': 'https://img.pokemondb.net/artwork/mewtwo.jpg',
  'types': ['Psychic'],
  '__typename': 'Pokemon'
}]
