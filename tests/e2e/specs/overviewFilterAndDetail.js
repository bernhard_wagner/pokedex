//   -- home -> overview -> filter pokes -> detail view -> ...
module.exports = {
  '@tags': ['test2'],
  'OverviewFilterAndDetail 1: navigate to pokedex': browser => {
    browser
      .url('https://pokedex-example.netlify.com/')
      .assert.elementPresent('Body')
      .waitForElementVisible('#app', 5000)
  },
  'OverviewFilterAndDetail 2: got to overview': browser => {
    browser
      .waitForElementVisible('a[href="/pokemon/overview"]', 5000)
      .click('a[href="/pokemon/overview"]')
      .waitForElementVisible('ul.v-expansion-panel', 5000)
      .waitForElementVisible('div.layout.row.wrap', 5000)
      .waitForElementVisible('a[href="/pokemon/Bulbasaur"]', 5000)
      .waitForElementVisible('a[href="/pokemon/Caterpie"]', 5000)

  },
  'OverviewFilterAndDetail 3: filter overview': browser => {
    browser
      .click('ul.v-expansion-panel')
      .elements('css selector', 'span[data-test="filter-chip"]', function(result) {
        // console.log(result);
        // console.log(result.value);
        
        result.value.forEach(function(element, i){
          if(i==0) {
            console.log(element.ELEMENT);
          } else {
            console.log('click type chip  ' + i);
            browser.elementIdClick(element.ELEMENT);
          }
        })
      })
      .waitForElementNotPresent('a[href="/pokemon/Bulbasaur"]', 5000)
      .waitForElementVisible('a[href="/pokemon/Caterpie"]', 5000)
      .pause(5000)
     
  },
  'OverviewFilterAndDetail 4: detail view': browser => {
    browser
    .click('a[href="/pokemon/Caterpie"]')
    .waitForElementVisible('div[aria-label="Butterfree"]', 5000)
    .pause(5000)
    .end()
  }
}