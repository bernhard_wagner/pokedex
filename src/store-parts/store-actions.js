import actionTypes from '../const/actionTypes'
import mutationTypes from '../const/mutationTypes'
import { getPokemonById, getPokemonByName, getPokemonList } from '../service/fetcher'

export default {
  async [actionTypes.FETCH_POKELIST]({ commit }) {
    const result = await getPokemonList();
    commit(mutationTypes.SET_POKELIST, result.data.pokemons);
  },
  async [actionTypes.FETCH_POKEMON]({ commit }, { name }) {
    const result = await getPokemonByName(name);
    commit(mutationTypes.SET_CURRENT_POKEMON, result.data.pokemon);
    commit(mutationTypes.SET_CURRENT_POKEMON_LOADED, true);
  },
  async [actionTypes.FETCH_POKEMON_ID]({ commit }, { id }) {
    const result = await getPokemonById(id);
    commit(mutationTypes.SET_CURRENT_POKEMON, result.data.pokemon);
    commit(mutationTypes.SET_CURRENT_POKEMON_LOADED, true);
  },
  async [actionTypes.RESET_CURRENT_POKEMON]({ commit }) {
    commit(mutationTypes.SET_CURRENT_POKEMON, {});
    commit(mutationTypes.SET_CURRENT_POKEMON_LOADED, false);
  },
}
