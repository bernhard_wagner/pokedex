import { createLocalVue, mount, shallowMount } from '@vue/test-utils'
import Pokegrid from '../../src/components/Pokegrid'
import Vuetify from 'vuetify'
import Vuex from 'vuex'
import PokemonList from '../_testData/pokemonList'
import StoreGetters from '../../src/store-parts/store-getters'


const localVue = createLocalVue();
localVue.use(Vuex)
localVue.use(Vuetify);

describe('PokemonTypeFilter.vue', () => {
  let store
  let state
  let actions

  const factory = (values = {}) => shallowMount(Pokegrid, {store, localVue, stubs: ['router-link', 'router-view', 'PokemonTypeFilter']});

  const listItemsAnchor = '[data-test="pokemon-list-item"]';
  const listFilterAnchor = '[data-test="pokemon-filter"]';

  beforeEach(() => {
    state = {
      pokemonList: PokemonList,
      currentPokemon: {}
    }

    store = new Vuex.Store({
      state,
      actions,
      getters: StoreGetters,
    })
  })

  it("should display pokemon list", () => {
      const wrapper = factory();
      const listItems = (wrapper.findAll(listItemsAnchor));

      expect(listItems.length).toBe(PokemonList.length);


  })

  it("should proper filter pokemon list", () => {
    const wrapper = factory();
    const filterComponent = wrapper.find(listFilterAnchor);

    filterComponent.vm.$emit('onFilter',['fire', 'poison', 'grass']);
    expect(wrapper.findAll(listItemsAnchor).length).toBe(120);

    filterComponent.vm.$emit('onFilter',['wrongFilterInput', 'poison', 'grass']);
    expect(wrapper.findAll(listItemsAnchor).length).toBe(130);

    filterComponent.vm.$emit('onFilter',[]);
    expect(wrapper.findAll(listItemsAnchor).length).toBe(PokemonList.length);
  })
})
