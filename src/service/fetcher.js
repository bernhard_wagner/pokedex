import {createClient as createApolloClient} from "../plugins/vue-apollo"
import pokemonList from "../graphQL/PokemonList.query.graphql";
import Pokemon from "../graphQL/Pokemon.query.graphql"
import pokemonDetail from "../graphQL/PokemonDetail.query.graphql";

const url = process.env.GRAPHQL_API_URL;
const apollo = createApolloClient();


/**
 * Gets a list of all available pokemon
 * @returns AxiosPromise<any> that resolves to a list of pokemon (only basic data)
 */
export function getPokemonList () {
  return apollo.query({query: pokemonList});
}

export function getPokemonByName (name) {
  return apollo.query({query: Pokemon, variables: {
    pokeName: name
  }});
}

/**
 * Gets an single pokemon by id
 * @returns AxiosPromise<any> that resolves to an pokemon object
 */
export function getPokemonById (id) {
  return apollo.query({query: pokemonDetail, variables: {id: id}})
}
