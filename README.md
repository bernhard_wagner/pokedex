# pokedex
The Project is about a simple online pokedex, where you can search for pokemon and get relevant information about them.

## Current Build
https://pokedex-example.netlify.com

## Backend API (GraphQL)
https://graphql-pokemon.now.sh/

## Team
Bernhard Wagner (S1810629012) \
Daniel H�lbling (S1810629004)

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
npm run test:e2e
```

### Lints and fixes files
```
npm run lint
```

### Plugins
```
- cli-service
- cli-plugin-babel
- cli-plugin-eslint
- cli-plugin-pwa
- cli-plugin-apollo
- cli-plugin-vuetify
```

### Dependencies
```
- vue
- vuex
- vuetify
- vue-router
- vue-apollo
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

## Outlook on updates
### Tech aspects
- Decide Detailview current pokemon query split into multiple queries per (child) component?

### Feature ideas
- pokemon of the day on homescreen
- PokeAttack comp. as table
- PokeEvolution comp. Links to other detail views
- PokeEvolution comp. Type Colors
- PokeTypeChart comp. Link to whole (filterable) Type Table (as overlay?)
- PokeTypeChart comp. Which type results in which weakness?

