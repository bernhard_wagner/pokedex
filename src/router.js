import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited. //TODO lazy load different routes
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue') //when webpackchungname is the same on different routes it bundles them together //see module methods on webpack
    }, //async loading also work on component level (async importing different components)
    {
      path: '/pokemon/overview',
      name: 'pokemon-overview',
      component: () => import(/* webpackChunkName: "overview" */ './views/Overview.vue'),
    },
    {
      path: '/pokemon/:name',
      name: 'pokemon-detail',
      component: () => import(/* webpackChunkName: "pokemonDetail" */ './views/PokemonDetail.vue'),
    },
    {
      path: '/pokemap',
      name: 'pokemon-map',
      component: () => import(/* webpackChunkName: "PokeMap" */ './views/PokeMap.vue'),
    },
  ],
  scrollBehavior (to, from, savedPosition) {
    return { x: 0, y: 0 }
  }
})
