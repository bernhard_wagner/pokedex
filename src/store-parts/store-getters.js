export default {
  filteredPokemonList: (state) => (filterTerm) => state.pokemonList.filter((item) => {return (item.name.toLowerCase().includes(filterTerm.toLowerCase()) || item.number === filterTerm)})
}
